# PuTTY
The popular SSH client


## Info

This is the official PuTTY client from Simon Tatham. It is here only for personal purposes.

## Install

putty-64bit-0.72-installer.msi  :  The official version with all features (64-bit) <br>
putty.exe  :  SSH and Telnet Client (64-bit) <br>
pscp.exe  :  SCP Client (64-bit) <br>
psftp.exe  :  SFTP Client (64-bit) <br>
pageant.exe  :  SSH authentication agent (64-bit) <br>
puttygen.exe  :  RSA/DSA key generation tool (64-bit) <br>

## Thank

Thanks to Simon Tatham for creating a very good SSH client for the Windows operating system.